package com.jalil.ejem08imagenesgrid;

import android.graphics.Bitmap;

public class Item {

    private Bitmap image = null;
    private String name = null;

    public Item(Bitmap image, String name) {
        this.image = image;
        this.name = name;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
