package com.jalil.ejem08imagenesgrid;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private GridView gridView = null;
    private ItemAdapter adapter = null;
    private ArrayList<Item> images = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        gridView = (GridView) findViewById(R.id.gridView);
        adapter = new ItemAdapter(this,R.layout.row_grid,fillImages());
        gridView.setAdapter(adapter);

    }

    private ArrayList<Item> fillImages() {
        int values = Integer.parseInt(getResources().getString(R.string.howMany));
        ArrayList<Item> lista = new ArrayList<>();

        for (int i = 0; i <= values; i++) {
            Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(),
                    getResources().getIdentifier("sample_"+i,
                            "drawable",
                            "com.jalil.ejem08imagenesgrid"));
            lista.add(new Item (bitmap,"sample_"+i));
        }
        return lista;
    }
}
