package com.jalil.ejem08imagenesgrid;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ItemAdapter extends ArrayAdapter{
    private Context context;
    private int layoutR;
    private ArrayList items;
    private int layout;

    public ItemAdapter(@NonNull Context context, int resource, @NonNull ArrayList items) {
        super(context, resource, items);
        this.layoutR = resource;
        this.context = context;
        this.items = items;
        this.layout = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View item = convertView;
        Item user = (Item)getItem(position);
        if (item == null)
            item = LayoutInflater.from(getContext()).inflate(R.layout.row_grid,parent,false);

        TextView tvName = (TextView) item.findViewById(R.id.mTextView);
        ImageView image = (ImageView) item.findViewById(R.id.mImageView);

        tvName.setText(user.getName());
        image.setImageBitmap(user.getImage());

        return super.getView(position, convertView, parent);
    }

    static class ViewHolder{
        TextView text;
        ImageView image;
    }
}
